import re
import platform

#=====| Hack for windows, because win does not support utf8 |====
if platform.system() == "Windows":
	outfile = open("./out.txt", "w", encoding="utf8")

def output(text = None):
	if text is None:
		if platform.system() == "Windows":
			outfile.write("\n")
		else:
			print()
	else:
		if platform.system() == "Windows":
			outfile.write(text + "\n")
		else:
			print(text)

#==================================================================
			
def compareLists(l1, l2):
	if len(l1) != len(l2):
		return False
		
	for e1 in l1:
		found = False
		for e2 in l2:
			if e1 == e2:
				found = True
				break
		if not found:
			return False
	return True			

class Print:
	def __init__(self, print_id):
		self.edition = None
		self.print_id = print_id
		self.partiture = False
		
	def __eq__(self, other):
		return self.print_id == other.print_id # Maybe compare the other stuff as well?

	def composition(self):
		return None if self.edition is None else self.edition.composition

	def format(self):
		output("Print Number: " + str(self.print_id))
		if self.edition is not None:
			self.edition.format()
		if self.partiture is not None:
			output("Partiture: " + ('yes' if self.partiture else 'no'))

class Edition:
	def __init__(self):
		self.composition = None
		self.authors = []
		self.name = None
		
	def __eq__(self, other):
		return self.name == other.name and self.composition == other.composition and compareLists(self.authors, other.authors)
		
	def format(self):
		str = ""
		for author in self.authors:
			if str != "":
				str += "; "
			str += author.formats()
		if str != "":
			output("Editor: " + str)
		if self.name is not None:
			output("Edition: " + self.name)
		if self.composition is not None:
			self.composition.format()

class Composition:
	def __init__(self):
		self.name = None
		self.incipit = None
		self.key = None
		self.genre = None
		self.year = None
		self.voices = []
		self.authors = []
	
	def __eq__(self, other):
		return self.name == other.name and self.incipit == other.incipit and self.key == other.key and self.genre == other.genre and self.year == other.year and compareLists(self.voices, other.voices) and compareLists(self.authors, other.authors)
		
	def format(self):
		string = ""
		for author in self.authors:
			if string != "":
				string += "; "
			string += author.formats()
		if string != "":
			output("Composer: " + string)
		if self.name is not None:
			output('Title: ' + self.name)
		if self.incipit is not None:
			output('Incipit: ' + self.incipit)
		if self.genre is not None:
			output('Genre: ' + self.genre)
		if self.key is not None:
			output('Key: ' + self.key)
		if self.year is not None:
			output('Composition Year: ' + str(self.year))
		i = 1
		for voice in self.voices:
			output('Voice ' + str(i) + ': ' + voice.formats())
			i += 1
			
class Voice:
	def __init__(self):
		self.name = None
		self.range = None
		
	def __eq__(self, other):
		return self.name == other.name and self.range == other.range
		
	def formats(self):
		ret = ''
		if self.range is not None:
			ret += self.range
			if self.name is not None:
				ret += ", " + self.name
		elif self.name is not None:
			ret += self.name
		return ret

class Person:
	def __init__(self):
		self.name = None
		self.born = None
		self.died = None
	
	def __eq__(self, other):
		return self.name == other.name and self.born == other.born and self.died == other.died
	
	def formats(self):
		str = self.name
		if self.born is not None:
			if self.died is not None:
				str += " (" + self.born + "--" + self.died + ")"
			else:
				str += " (" + self.born + "--)"
		elif self.died is not None:
			str += " (--" + self.died + ")"
		return str

class Collections:
	def __init__(self):
		self.people = {}
		self.voices = {}
		self.editions = {}
		self.compositions = {}
	
	def getAbstract(self, dict, obj, objkey):
		if objkey is None:
			return obj
		if objkey in dict:
			arr = dict[objkey]
			for i in arr:
				if i == obj:
					return i	
			dict[objkey].append(obj)
			return obj
		else:		
			dict[objkey] = [ obj ]
			return obj
	
	def getPerson(self, person):
		return self.getAbstract(self.people, person, person.name)
		
	def getVoice(self, voice):
		return self.getAbstract(self.voices, voice, voice.name)
			
	def getEdition(self, edition):
		return self.getAbstract(self.editions, edition, edition.name)
	
	def getComposition(self, composition):
		return self.getAbstract(self.compositions, composition, composition.name)
	
def pareseEditor(editor):
	parts = editor.split(",")
	editors = []
	for i in range(0, len(parts)):
		if i == len(parts) - 1:
			if len(parts) == 1:
				editors.append(parts[i])
			else:
				editors[len(editors) - 1] += ", " + parts[i]
		elif i % 2 == 0:
			editors.append(parts[i])
		else:
			editors[int(i/2)] += ", " + parts[i]
	return editors
	
def parsePrintInfo(info, nPrint, collections):
	# My regex handled more than it should have :/
	# rcomp = re.compile(r"^ *([^\(]*[^\( ]) *\(([0-9/]*)-*\+?([0-9/]*)\)$")
	rcomp = re.compile(r"^ *([^\(]*[^\( ]) *\((\*?[0-9]*)-*\+?([0-9]*)\)$")
	rvoice = re.compile(r"^ *([^- ,]+)--([^- ,]+) *(?:[,;]? *(.+))?$")
	ryear = re.compile(r"(?:.*[^0-9]+)*([0-9]+)[^0-9]*")
	# Print
	if 'Partiture' in info:
		if 'yes' in info['Partiture']:
			nPrint.partiture = True
		elif 'no' in info['Partiture']:
			nPrint.partiture = False
	
	# Edition 
	edition = Edition()
	
	if 'Edition' in info:
		edition.name = info['Edition']
		
	if 'Editor' in info:
		editors = []
		for i in info['Editor'].split(", continuo by"):
			for j in i.split(", continuo:"):
				editors += pareseEditor(j.strip())

		for comp in editors:
			comp = comp.strip()
			person = Person()
			
			m = rcomp.match(comp)
			if m:
				person.name = m.group(1)
				if len(m.group(2)) > 0:
					person.born = m.group(2)
				if len(m.group(3)) > 0:	
					person.died = m.group(3)
			else:	
				person.name = comp
				
			obj = collections.getPerson(person)
			edition.authors.append(obj)
		
	# Composition
	composition = Composition()
	
	if 'Title' in info:
		composition.name = info['Title']
		
	if 'Incipit' in info:
		composition.incipit = info['Incipit']
		
	if 'Key' in info:
		composition.key = info['Key']
		
	if 'Genre' in info:
		composition.genre = info['Genre']
		
	if 'Composer' in info:
		for comp in info['Composer'].split(";"):
			comp = comp.strip()
			person = Person()
			
			m = rcomp.match(comp)
			if m:
				person.name = m.group(1)
				if len(m.group(2)) > 0:
					person.born = m.group(2)
				if len(m.group(3)) > 0:	
					person.died = m.group(3)
			else:	
				person.name = comp
			obj = collections.getPerson(person)
			composition.authors.append(obj)
	
	if 'Composition Year' in info:
		m = ryear.match(info['Composition Year'])
		if m:
			year = m.group(1)
			if len(year) == 4:
				composition.year = int(year)

	for i in range(1, 10):
		if ('Voice ' + str(i)) in info:
			voice = Voice()
			voicestr = info['Voice ' + str(i)]
			m = rvoice.match(voicestr)
			if m:
				if len(m.group(1)) > 0 and len(m.group(2)) > 0:
					voice.range = m.group(1) + '--' + m.group(2)
				if m.group(3) is not None:
					voice.name = m.group(3)
			else:
				voice.name = voicestr
				
			obj = collections.getVoice(voice)
			composition.voices.append(obj)
	
	edition.composition = collections.getComposition(composition)
	nPrint.edition = collections.getEdition(edition)
			
def load(filename):
	r = re.compile(r"^([^:]+): *([^ ].*)$")
	f = open( filename, 'r', encoding="utf8" )
	
	info = {}
	prints = []
	collections = Collections()

	for line in f:
		if line == "\n":
			if 'Print Number' in info:
				nPrint = Print(int(info['Print Number']))
				parsePrintInfo(info, nPrint, collections)
				prints.append(nPrint)

			info = {}
		else:
			m = r.match(line)
			if m:
				if len(m.group(2).rstrip()) >= 1:
					if m.group(1) in info:
						output("Error: " + m.group(1))
					info[m.group(1)] = m.group(2).rstrip()
			else:
				output("Trouble: " + line)
				
	if 'Print Number' in info:
		nPrint = Print(int(info['Print Number']))
		parsePrintInfo(info, nPrint, collections)
		prints.append(nPrint)			
	return prints
