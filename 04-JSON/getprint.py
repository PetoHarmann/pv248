import sqlite3
import sys
import json


print_id = sys.argv[1]
conn = sqlite3.connect('./scorelib.dat')
c = conn.cursor()
c.execute('PRAGMA foreign_keys = true;')

res = c.execute('''SELECT person.name, person.born, person.died FROM person 
						INNER JOIN score_author ON score_author.composer = person.id 
						INNER JOIN edition ON edition.score = score_author.score 
						INNER JOIN print ON print.edition = edition.id 
					WHERE print.id = ?''', (print_id,))

people = []
for e in res:
	person = {}
	if e[0] is not None:
		person['name'] = e[0]
	if e[1] is not None:
		person['born'] = e[1]
	if e[2] is not None:
		person['died'] = e[2]

	people.append(person)
	
print(json.dumps(people, ensure_ascii=False))





