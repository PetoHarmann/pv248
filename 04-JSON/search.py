import sqlite3
import sys
import json

composer_name = sys.argv[1]
conn = sqlite3.connect('./scorelib.dat')
c = conn.cursor()
c.execute('PRAGMA foreign_keys = true;')

res = c.execute('''SELECT score_author.score, person.name, person.born, person.died FROM score_author
						INNER JOIN person ON score_author.composer = person.id''')
						
composers = {}
for e in res:
	if e[0] is not None:
		composer = {}
		if e[1] is not None:
			composer['Name'] = e[1]
		if e[2] is not None:
			composer['Born'] = e[2]
		if e[3] is not None:
			composer['Died'] = e[3]
		
		if e[0] in composers:
			composers[e[0]].append(composer)
		else:
			composers[e[0]] = [ composer ]
		

		
res = c.execute('''SELECT edition_author.edition, person.name, person.born, person.died FROM edition_author
						INNER JOIN person ON edition_author.editor = person.id''')
				
editors = {}
for e in res:
	if e[0] is not None:
		editor = {}
		if e[1] is not None:
			editor['Name'] = e[1]
		if e[2] is not None:
			editor['Born'] = e[2]
		if e[3] is not None:
			editor['Died'] = e[3]
		
		if e[0] in editors:
			editors[e[0]].append(editor)
		else:
			editors[e[0]] = [ editor ]
			
			


res = c.execute('''SELECT voice.score, voice.number, voice.name, voice.range FROM voice''')
				
voices = {}
for e in res:
	if e[0] is not None:
		voice = {}
		if e[1] is not None:
			voice['Number'] = e[1]
		if e[2] is not None:
			voice['Name'] = e[2]
		if e[3] is not None:
			voice['Range'] = e[3]
		
		if e[0] in voices:
			voices[e[0]].append(voice)
		else:
			voices[e[0]] = [ voice ]


			
			

res = c.execute('''	SELECT 
						person.name, print.id, 
						score.id, score.name, score.genre, score.key, score.year,
						edition.name, edition.id, print.partiture, score.incipit
					FROM 
						person
						INNER JOIN score_author ON score_author.composer = person.id
						INNER JOIN score ON score.id = score_author.score
						INNER JOIN edition ON edition.score = score.id
						INNER JOIN print ON print.edition = edition.id
					WHERE person.name LIKE \'%\' || ? || \'%\'''', (composer_name,))
	
person = {}	
for e in res:
	m_print = {}
	
	if e[1] is not None:
		m_print['Print Number'] = e[1]
		
	if e[2] is not None and e[2] in composers:
		m_print['Composer'] = []
		for i in composers[e[2]]:
			m_print['Composer'].append(i)
		
	if e[3] is not None:
		m_print['Title'] = e[3]
	
	if e[4] is not None:
		m_print['Genre'] = e[4]
		
	if e[5] is not None:
		m_print['Key'] = e[5]
		
	if e[6] is not None:
		m_print['Composition Year'] = e[6]
		
	if e[7] is not None:
		m_print['Edition'] = e[7]
		
	if e[8] is not None and e[8] in editors:
		m_print['Editor'] = []
		for i in editors[e[8]]:
			m_print['Editor'].append(i)
		
	if e[2] is not None and e[2] in voices:
		m_print['Voice'] = {}
		for i in voices[e[2]]:
			m_print['Voice'][i['Number']] = {}
			if 'Name' in i:
				m_print['Voice'][i['Number']]['Name'] = i['Name']
			if 'Range' in i:
				m_print['Voice'][i['Number']]['Range'] = i['Range']
		
	if e[9] is not None:
		m_print['Partiture'] = True if e[9] == 'Y' else False
		
	if e[10] is not None:
		m_print['Incipit'] = e[10]	
		
	if e[0] in person:
		person[e[0]].append(m_print)
	else:
		person[e[0]] = [ m_print ]
	
print(json.dumps(person, indent=4, separators=(',', ': '), ensure_ascii=False))








