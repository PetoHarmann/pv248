import sys
from enum import Enum
import time

class KarelInternalException(Exception):
	def __init__(self, text):
		self.text = text
		
	def __str__(self):
		return self.text
		
class KarelUserException(Exception):
	def __init__(self, text):
		self.text = text
		
	def __str__(self):
		return self.text
		
class Direction(Enum):
	North = 1
	East = 2
	South = 3
	West = 4

class Tile:
	def __init__(self):
		self.wall = False
		self.marks = 0
		
class Position:
	def __init__(self, x = 0, y = 0):
		self.x = x
		self.y = y
		self.direction = Direction.North
		
	def setDir(self, d):
		if d == 'n':
			self.direction = Direction.North
		elif d == 'e':
			self.direction = Direction.East
		elif d == 's':
			self.direction = Direction.South
		elif d == 'w':
			self.direction = Direction.West
			
	def __str__(self):
		if self.direction == Direction.North:
			return '^'
		elif self.direction == Direction.East:
			return '>'
		elif self.direction == Direction.South:
			return 'ˇ'
		elif self.direction == Direction.West:
			return '<'

class Map:
	def __init__(self, h = 0, w = 0, position = Position()):
		if position is not None:
			self.start_position = position
			self.position = Position(position)
		else:
			self.start_position = None
			self.position = None
	
		self.height = h
		self.width = w
		self.tiles = []
		
		if h > 0 and w > 0:
			self.resizeMap(h, w)
	
	def resizeMap(self, h, w):
		self.height = h
		self.width = w
	
		for i in range(0, h):
			t = []
			for j in range(0, w):
				t.append(Tile())
			self.tiles.append(t)
			
	#def setPosition(self, position):
	#	self.position = position
			
	def put(self):
		self.tiles[self.position.x][self.position.y].marks += 1
		
	def pick(self):
		if self.tiles[self.position.x][self.position.y].marks > 0:
			self.tiles[self.position.x][self.position.y].marks -= 1
		else:
			raise KarelUserException('There are no marks to pick!')
		
	def right(self):
		if self.position.direction == Direction.North:
			self.position.direction = Direction.East
		elif self.position.direction == Direction.East:
			self.position.direction = Direction.South
		elif self.position.direction == Direction.South:
			self.position.direction = Direction.West
		elif self.position.direction == Direction.West:
			self.position.direction = Direction.North
			
	def left(self):
		if self.position.direction == Direction.North:
			self.position.direction = Direction.West
		elif self.position.direction == Direction.East:
			self.position.direction = Direction.North
		elif self.position.direction == Direction.South:
			self.position.direction = Direction.East
		elif self.position.direction == Direction.West:
			self.position.direction = Direction.South
			
	def move(self):
		if self.position.direction == Direction.North:
			self.position.x -= 1
		elif self.position.direction == Direction.South:
			self.position.x += 1
		elif self.position.direction == Direction.East:
			self.position.y += 1
		elif self.position.direction == Direction.West:
			self.position.y -= 1
			
		if self.position.x < 0 or self.position.x >= self.height or self.position.y < 0 or self.position.y >= self.width:
			raise KarelUserException('Out of map!')
			
		if self.tiles[self.position.x][self.position.y].wall == True:
			raise KarelUserException('Crashed into wall!')
		
	def checkMarks(self):
		return self.tiles[self.position.x][self.position.y].marks > 0
		
	def checkWall(self):
		x = self.position.x
		y = self.position.y
		if self.position.direction == Direction.North:
			x -= 1
		elif self.position.direction == Direction.South:
			x += 1
		elif self.position.direction == Direction.East:
			y += 1
		elif self.position.direction == Direction.West:
			y -= 1
		else:
			raise KarelInternalException('Invalid direction')
			
		if x < 0 or x >= self.height or y < 0 or y >= self.width:
			return True
			
		return self.tiles[x][y].wall
		
	def __str__(self):
		ret_str = ''
		for i in range(0, self.height):
			for j in range(0, self.width):
				if self.position.x == i and self.position.y == j:
					ret_str += str(self.position)
				elif self.tiles[i][j].wall:
					ret_str += '#'
				elif self.tiles[i][j].marks == 0:
					ret_str += ' '
				else:
					ret_str += str(self.tiles[i][j].marks)
			ret_str += '\n'
		return ret_str

	def parseFile(self, filename):
		with open( filename, 'r', encoding="utf8" ) as f:
			i = -3
			for line in f:
				i += 1
				if i >= 0:
					for j in range(0, self.width):
						if line[j] == '#':
							self.tiles[i][j].wall = True
						elif line[j] == ' ' or line[j] == '0':
							pass
						else:
							try:
								m = int(line[j])
								self.tiles[i][j].marks = m
							except:
								raise KarelUserException('Bad map format on line ' + str(i) + ' symbol ' + str(j))
							
				elif i == -2:
					d = line.strip().split(' ')	
					self.resizeMap(int(d[0]), int(d[1]))
				elif i == -1:
					self.start_position = Position()
				
					d = line.strip().split(' ')	
					self.start_position.x = int(d[0])
					self.start_position.y = int(d[1])
					self.start_position.setDir(d[2])
					self.position = self.start_position


class MachineCode:
	class Inst(Enum):
		NOP 	= 0
		PROC 	= 1
		RETN	= 2
		JNW		= 3
		JNM		= 4
		HALT	= 5
		MOVE	= 6
		LEFT	= 7
		RIGHT	= 8
		PICK	= 9
		PUT		= 10
		JMP		= 11
		CALL	= 12

	def __init__(self):
		self.symbols = {}
		self.forward_symbols = {}
		self.code = []
		self.state = self.globalState;
		self.jump  = -1
		
		self.ops = {
			'DEFINE'	: None,
			'ELSE'		: None,
			'IFWALL' 	: (self.Inst.JNW, 	self.IfState, 		self.jumpAddr),
			'IFMARK' 	: (self.Inst.JNM, 	self.IfState, 		self.jumpAddr),
			'SKIP'   	: (self.Inst.NOP,	None,				None),
			'BREAK'	 	: (self.Inst.RETN,	None,				None),
			'HALT'	 	: (self.Inst.HALT,	None,				None),
			'MOVE'		: (self.Inst.MOVE,	None,				None),
			'LEFT'		: (self.Inst.LEFT,	None,				None),
			'RIGHT'		: (self.Inst.RIGHT,	None,				None),
			'PICKUP'	: (self.Inst.PICK,	None,				None),
			'PUTDOWN'	: (self.Inst.PUT,	None,				None),
			'END'		: (self.Inst.RETN,	self.globalState, 	None)
		}
		
	def __str__(self):
		return 'SYMBOLS:\n' + str(self.symbols) + '\n\nCODE:\n' + str(self.code)
		
	def parseLine(self, line):
		ret = []
		temp = ''
		for i in range(0, len(line)):
			if line[i] == ' ':
				if temp != '':
					ret.append(temp)
					temp = ''
			else:
				temp += line[i].strip()
		if temp != '':
			ret.append(temp)
		return ret
		
	def jumpAddr(self):
		if self.jump != -1:
			raise KarelUserException('IF inside of IF or ELSE on line ' + str(line))
			
		self.jump = len(self.code)
		self.code.append(-1)
		
	def fillAddr(self, llist, addr):
		i = llist
		while i != -1:
			t = self.code[i]
			self.code[i] = addr
			i = t
		
	def parseFile(self, filename):
		with open( filename, 'r', encoding="utf8" ) as f:
			i = 0
			for line in f:
				for w in self.parseLine(line):
					self.state(w, i)
				i += 1
			if self.state != self.globalState:
				raise KarelUserException('Expected END before end of file.')
				
			if len(self.forward_symbols) > 0:
				raise KarelUserException('Symbol not resolved: ' + str(next(iter(self.forward_symbols.keys()))))
				
	def globalState(self, word, line):
		if word == 'DEFINE':
			self.state = self.procName
			self.code.append(self.Inst.PROC)
		else:
			raise KarelUserException('Unexpected ' + word + ' on line ' + str(line))
		
	def procName(self, word, line):
		if word not in self.ops:
			if word in self.symbols:
				raise KarelUserException('Symbol already defined ' + word + ' on line ' + str(line))
			elif word in self.forward_symbols:
				self.fillAddr(self.forward_symbols[word], len(self.code) - 1)
				del self.forward_symbols[word]
				self.symbols[word] = len(self.code) - 1
				self.state = self.procState
			else:
				self.symbols[word] = len(self.code) - 1
				self.state = self.procState
		
	def procState(self, word, line):
		if word not in self.ops:
			# CALL to the function
			self.code.append(self.Inst.CALL)
			
			# Find the function address
			if word in self.symbols:
				self.code.append(self.symbols[word])
			elif word in self.forward_symbols:
				t = self.forward_symbols[word]
				self.forward_symbols[word] = len(self.code)
				self.code.append(t)
			else:
				self.forward_symbols[word] = len(self.code)
				self.code.append(-1)
		else:
			op = self.ops[word]
			if op is None:
				raise KarelUserException('Unexpected ' + word + ' on line ' + str(line))
			self.code.append(op[0])
			if op[1] is not None:
				self.state = op[1]
			if op[2] is not None:
				op[2]()
				
	def IfState(self, word, line):
		self.procState(word, line)
		if self.state != self.IfState:
			raise KarelUserException('Unexpected ' + word + ' on line ' + str(line))
		else:
			self.state = self.BranchState
	
	def BranchState(self, word, line):
		if word == 'ELSE':
			# Jump after the JMP instruction into ELSE block (JMP has 2 cells)
			self.code[self.jump] = len(self.code) + 2
			# Generate JMP with address placeholder
			self.code.append(self.Inst.JMP)
			self.jump = len(self.code)
			self.code.append(-1)
			# Set Else State
			self.state = self.ElseState
		else:
			# IF will jump to first inst after the end of IF block (if false)
			self.code[self.jump] = len(self.code)
			self.jump = -1
			
			# Process normal instruction
			self.state = self.procState
			self.procState(word, line)
			
	def ElseState(self, word, line):
		self.procState(word, line)
		if self.state != self.ElseState:
			raise KarelUserException('Unexpected ' + word + ' on line ' + str(line))
		else:
			# Jump to first inst after end of ELSE block
			self.code[self.jump] = len(self.code)
			self.jump = -1
			# Normal ops from now
			self.state = self.procState

class Machine:
	def __init__(self, code, map):
		self.code 	= code.code
		self.map	= map
		if 'MAIN' not in code.symbols:
			raise KarelUserException('No MAIN found. No entry point.')
		self.cip = code.symbols['MAIN']
		self.stack = [ -1 ]
	
		self.inst = {
			MachineCode.Inst.NOP 	: self.NOP,
			MachineCode.Inst.PROC 	: self.PROC,
			MachineCode.Inst.RETN	: self.RETN,
			MachineCode.Inst.JNW	: self.JNW,
			MachineCode.Inst.JNM	: self.JNM,
			MachineCode.Inst.HALT	: self.HALT,
			MachineCode.Inst.MOVE	: self.MOVE,
			MachineCode.Inst.LEFT	: self.LEFT,
			MachineCode.Inst.RIGHT	: self.RIGHT,
			MachineCode.Inst.PICK	: self.PICK,
			MachineCode.Inst.PUT	: self.PUT,
			MachineCode.Inst.JMP	: self.JMP,
			MachineCode.Inst.CALL	: self.CALL
		}
	
	def NOP(self):
		self.cip += 1
	
	def PROC(self):
		self.cip += 1
		
	def RETN(self):
		self.cip = self.stack.pop()
		
	def JNW(self):
		if map.checkWall():
			self.cip += 2
		else:
			self.cip = self.code[self.cip+1]
		
	def JNM(self):
		if map.checkMarks():
			self.cip += 2
		else:
			self.cip = self.code[self.cip+1]
		
	def HALT(self):
		self.cip = -1
		
	def MOVE(self):
		map.move()
		self.cip += 1
		
	def LEFT(self):
		map.left()
		self.cip += 1
		
	def RIGHT(self):
		map.right()
		self.cip += 1
		
	def PICK(self):
		map.pick()
		self.cip += 1
		
	def PUT(self):
		map.put()
		self.cip += 1
		
	def JMP(self):
		self.cip = self.code[self.cip + 1]
	
	def CALL(self):
		self.stack.append(self.cip + 2)
		self.cip = self.code[self.cip + 1]
	
	def step(self):
		if(self.cip == -1):
			raise KarelUserException('End of program already reached!')
			
		self.inst[self.code[self.cip]]()
	
	def execute(self):
		while self.cip != -1:
			self.step()
			
	def running(self):
		return self.cip != -1
		
	def printMap(self):
		print(machine.map)

try:
	mapfile = sys.argv[1]
	progfile = sys.argv[2]

	map = Map()
	map.parseFile(mapfile)

	machine_code = MachineCode()
	machine_code.parseFile(progfile)

	machine = Machine(machine_code, map)

	print(machine.map)
	while machine.running():
		time.sleep(0.1)
		machine.step()
		machine.printMap()
		
except KarelUserException as e:
	print(e)

			