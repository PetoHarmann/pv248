import sys
import re
from collections import Counter

r = re.compile(r"([^:]+): *([^ ].*)")

f = open( sys.argv[1], 'r', encoding="utf8" )

#temp_file = open("./composers.txt", "w", encoding="utf8")

info = {}
songs = {}
number = 0
for line in f:
	if line == "\n":
		songs[number] = info
		info = {}
		number += 1
	else:
		m = r.match(line)
		if m:
			if len(m.group(2)) >= 1:
                                if m.group(1) in info:
                                        print("Error (" + str(number) + ": " + m.group(1))
                                info[m.group(1)] = m.group(2)
		else:
			print("Trouble: " + line)

if "Print Number" in info:
        songs[number] = info
        info = {}
        number += 1

if sys.argv[2] == "composer":
	r = re.compile(r"^ *([^\(]*[^\( ]) *\([0-9\-/+]+\).*$")
	comp_num = Counter()
	for key, info in songs.items():
		if 'Composer' in info:
			for comp in info['Composer'].split(";"):
                                comp = comp.strip()
                                if len(comp) > 0:
                                        m = r.match(comp);
                                        if m and len(m.group(1)) > 0:
                                                comp = m.group(1)
                                        else:
                                                comp = comp
                                        comp_num[comp] += 1

	for comp, num in comp_num.items():
		print(comp + ": " + str(num))
		#temp_file.write(comp + ": " + str(num) + "\n")
elif sys.argv[2] == "century":
	r = re.compile(r"(?:.*[^0-9]+)*([0-9]+)[^0-9]*")
	cent_num = Counter()
	for key, info in songs.items():
		if 'Composition Year' in info:
			m = r.match(info['Composition Year'])
			if m:
				year = m.group(1)
				if len(year) == 4:
					cent = int(year[0:2]) + 1
				elif len(year) == 3:
					cent = int(year[0:1]) + 1
				else:
					cent = int(year)
				cent_num[cent] += 1
	for cent, num in cent_num.items():
		print(str(cent) + "th century: " + str(num))
			
