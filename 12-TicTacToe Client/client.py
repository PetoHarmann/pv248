import sys, json

import urllib
import urllib.request
from urllib.parse import urlencode, ParseResult
import urllib.error
import http.server
from socket import timeout as timeout_exept

from time import sleep


class TTTExcept(Exception):
	def __init__(self, code, text):
		self.code = code
		self.text = text
		
	def __int__(self):
		return self.code
	
	def __str__(self):
		return self.text
def sendRequest(path, query):
	host = sys.argv[1]
	port = sys.argv[2]
	headers = {}

	query_b = urlencode(query)
	url = ParseResult(scheme='http', netloc=host + ':' + port, path=path, params='', query=query_b, fragment='').geturl()

	req = urllib.request.Request(url = url, headers = headers)
	try:
		with urllib.request.urlopen(req, timeout = 6) as response:
			resp_headers = response.getheaders()
			resp_content = response.read()
			
			try:
				content_json = json.loads(str(resp_content, 'utf-8'))
				return content_json
			except:
				raise TTTExcept(1, 'Bad JSON format!')
				
	except (urllib.error.HTTPError, urllib.error.URLError) as error:
		raise TTTExcept(3, 'Data not retrieved because %s\nURL: %s', error, url)
	except timeout_exept:	
		raise TTTExcept(2, 'Timeout!')

def getGame():
	data = sendRequest('/list', {})
	games = data['games']
	for i in games:
		if i['state'] == 1:
			print(str(i['id']) + " " + str(i['name']))
	print('Enter number of game or new:')
	line = sys.stdin.readline().rstrip('\n')
	
	if line == "new":
		data = sendRequest('/start', { 'name' : 'Unnamed Game'})
		return (int(data['id']), 1)
	else:
		data = sendRequest('/join', { 'game' : int(line) })
		if data['status'] == 'ok':
			return (int(data['id']), int(data['player']))
		else:
			print(data['message'])
			return getGame()

def printBoard(board):
	sym = ['_','x','o']
	for i in board:
		p_str = ''
		for j in i:
			p_str += sym[j]
		print(p_str)
		
def updateBoard(data, player):
	if 'board' in data:
		printBoard(data['board'])
	
	if 'winner' in data:
		winner = data['winner']
		if winner == player:
			print('you win')
		elif winner == 0:
			print('draw')
		else:
			print('you lose')
		return winner
	elif data['next'] != player:
		print('waiting for the other player')
		
def playGame(id, player):
	winner = None
	state = None
	
	while winner is None:
		data = sendRequest('/status', { 'game' : id })
		if state != data:
			winner = updateBoard(data, player)
			state = data
		
		if 'next' in data and data['next'] == player:
			x = None
			while x is None:
				try:
					if player == 1:
						print('your turn (x):')
					else:
						print('your turn (o):')
					line = sys.stdin.readline().rstrip('\n')
					(x, y) = [t(s) for t,s in zip((int,int),line.split())]
				
					data = sendRequest('/play', { 'game' : id, 'player' : player, 'x' : x, 'y' : y })
				
					if data['status'] == 'ok':
						data = sendRequest('/status', { 'game' : id })
						winner = updateBoard(data, player)
						state = data
						sleep(0.8)
					else:
						print(data['message'])
						x = None
				except:
					print('invalid input')
		else:
			sleep(0.8)
			
	
try:
	id, player = getGame()
	playGame(id, player)
except:
	pass


