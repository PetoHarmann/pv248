import numpy as np
import sys
import re


r = re.compile(r"([+\-]?) *([0-9]*) *([a-z])")
f = open( sys.argv[1], 'r', encoding="utf8" )

mat = {}
res = []

line_num = 0

for line in f:
	if len(line) > 2:
		a = line.split("=")
		
		for i in re.findall('[+\-]? *[0-9]*[a-z]', a[0]):
			m = r.match(i)
			c = 1
			if m:
				if m.group(1) is not None and m.group(1) == '-':
					c = -1
				if m.group(2) is not None and len(m.group(2)) >= 1:
					c *= int(m.group(2))
				if m.group(3) in mat:
					mat[m.group(3)][line_num] = c
				else:
					mat[m.group(3)] = { line_num : c }
			
		res.append(int(a[1]))
		line_num += 1

num_var = {}
var_num = {}

i = 0
for k, v in mat.items():
	num_var[i] = k
	var_num[k] = i
	i += 1
	
arr 		= np.zeros((line_num, i+1))
for k1, v1 in mat.items():
	for k2, v2 in v1.items():
		arr[k2][var_num[k1]] = v2

arr[:,i] = res
		
var1 = np.linalg.matrix_rank(arr[:,:-1])
var2 = np.linalg.matrix_rank(arr)

if var1 == var2:
	if var1 < i:
		print("solution space dimension: " + str(i-var1))
	else:
		out_str = "solution: "
		sol = []
		if var1 < line_num:
			lambdas, V =  np.linalg.eig(arr.T)
			sol = np.linalg.solve(arr[abs(lambdas) > 0.000001,:-1], arr[abs(lambdas) > 0.000001,i])
		else:
			sol = np.linalg.solve(arr[:,:-1], res)
		
		j = 0
		for key, value in sorted(var_num.items()):
			#for var_val in sol:
			if j != 0:
				out_str += ", "
			out_str += key + " = " + str(sol[value])
			j += 1
		print(out_str)
else:
	print("no solution")
	