import scorelib
import sys
import sqlite3
import os

#try:
#	os.remove(sys.argv[2])
#except FileNotFoundError:
#	a = 1
	
prints, collections = scorelib.load(sys.argv[1])

conn = sqlite3.connect(sys.argv[2])
c = conn.cursor()
c.execute('PRAGMA foreign_keys = true;')

with open( 'scorelib.sql', 'r', encoding="utf8" ) as f:
	content = f.read()
	c.executescript(content)

for v in collections.people.items():
	c.execute('INSERT INTO person (name, born, died) VALUES (?, ?, ?)', (v.name, v.born, v.died))
	v.id = c.lastrowid
	
for v in collections.compositions.items():
	c.execute('INSERT INTO score (name, genre, key, incipit, year) VALUES (?, ?, ?, ?, ?)', (v.name, v.genre, v.key, v.incipit, v.year))
	v.id = c.lastrowid
	
	for a in v.authors:
		conn.execute('INSERT INTO score_author (score, composer) VALUES (?, ?)', (v.id, a.id))
	
	num = 1
	for a in v.voices:
		c.execute('INSERT INTO voice (`number`, score, `range`, name) VALUES (?, ?, ?, ?)', (num, v.id, a.range, a.name))
		num += 1

for v in collections.editions.items():
	c.execute('INSERT INTO edition (score, name) VALUES (?, ?)', (v.composition.id, v.name))
	v.id = c.lastrowid
	
	for a in v.authors:
		conn.execute('INSERT INTO edition_author (edition, editor) VALUES (?, ?)', (v.id, a.id))

for v in prints:
	c.execute('INSERT INTO print (id, partiture, edition) VALUES (?, ?, ?)', (v.print_id, 'Y' if v.partiture else 'N', v.edition.id if v.edition is not None else None))

	
conn.commit()
conn.close()
	