import wave, struct, numpy, math, sys

a4 = float(sys.argv[1])
f = wave.open(sys.argv[2], 'rb')

tone_step = 2.0**(1.0/12.0)

data = []
if f.getnchannels() == 1:
	frames = f.readframes(f.getnframes())
	for i in range(0, f.getnframes()*2, 2):
		data.append(struct.unpack("h", bytearray(frames[i:(i+2)]))[0])
elif f.getnchannels() == 2:
	frames = f.readframes(f.getnframes())
	for i in range(0, f.getnframes()*4, 4):
		data.append((struct.unpack("h", bytearray(frames[i:(i+2)]))[0] + struct.unpack("h", bytearray(frames[(i+2):(i+4)]))[0])/2)

framerate = f.getframerate()
framewindow = f.getframerate()//10
numwindows = f.getnframes()//framewindow - 9
maxarray = []

#print("Length = " + str(f.getnframes()/framerate))

for i in range(0, numwindows):
	win = data[i*framewindow:((i+10)*framewindow)]
	proc = numpy.abs(numpy.fft.rfft(win))
	mean = numpy.mean(proc)
	
	maxarray.append([])
	peak_start = None
	for j in range(0,len(proc)):
		if(proc[j] >= 20 * mean):
			if peak_start is None:
				peak_start = j
		elif peak_start is not None:
			mid_point = (j - peak_start) / 2 + peak_start
			peak_max = peak_start
			for k in range(peak_start+1, j):
				if proc[k] > proc[peak_max]:
					peak_max = k
				elif proc[k] == proc[peak_max] and abs(k - mid_point) < abd(peak_max - mid_point):
					peak_max = k
			maxarray[i].append(peak_max)
			peak_start = None
	maxarray[i].sort(reverse=True, key = lambda x: proc[x])

def getMy(x, y):
	if y < len(x):
		return x[y]
	else:
		return None

pitchNameTable = ['c','cis','d','es','e','f','fis','g','gis','a','bes','b']
tonesTable = []
tonesNameTable = []

for j in range(0, len(pitchNameTable)):
	tonesNameTable.append(pitchNameTable[j][0].upper() + pitchNameTable[j][1:] + ',,')
	tone_freq = a4 * 2.0 ** (-4.0 + (j-9.0)/12.0)
	tonesTable.append(tone_freq)

for j in range(0, len(pitchNameTable)):
	tonesNameTable.append(pitchNameTable[j][0].upper() + pitchNameTable[j][1:] + ',')
	tone_freq = a4 * 2.0 ** (-3.0 + (j-9.0)/12.0)
	tonesTable.append(tone_freq)

for j in range(0, len(pitchNameTable)):
	tonesNameTable.append(pitchNameTable[j][0].upper() + pitchNameTable[j][1:])
	tone_freq = a4 * 2.0 ** (-2.0 + (j-9.0)/12.0)
	tonesTable.append(tone_freq)

postfix = ''
for i in range(3, 12):
	for j in range(0, len(pitchNameTable)):
		tonesNameTable.append(pitchNameTable[j] + postfix)
		tone_freq = a4 * 2.0 ** (i - 4.0 + (j-9.0)/12.0)
		tonesTable.append(tone_freq)
	postfix += '’'

def toneStr(freq):
	for i in range(0, len(tonesTable)):
		if freq < tonesTable[i]:
			base = 2.0 ** (1.0/1200.0)
			# tonesTable[i-1]*base^x = freq
			cent = round(math.log(freq/tonesTable[i-1],base))
			#print("round(math.log(" + str(freq) + "/" + str(tonesTable[i-1]) + ", " + str(base) + ")) == " + str(cent))
			if cent > 50:
				return tonesNameTable[i] + "-" + str(100 - cent)
			else:
				return tonesNameTable[i-1] + "+" + str(cent)
	print("Error: Out of range!")
		
def printPitch(start, end, p1, p2, p3):
	pstr = str(start//10) + "." + str(start % 10) + "-" + str(end//10) + "." + str(end % 10)
	if p1 is not None:
		pstr += " " + toneStr(p1)
	if p2 is not None:
		pstr += " " + toneStr(p2)
	if p3 is not None:
		pstr += " " + toneStr(p3)
	print(pstr)
		
intStart = None
p1 = None
p2 = None
p3 = None
for i in range(0, numwindows):
	cur_peaks = None
	if len(maxarray[i]) > 3:
		cur_peaks = maxarray[i][:3]
	else:
		cur_peaks = maxarray[i]
	cur_peaks.sort()
		
	if p1 == getMy(cur_peaks, 0) and p2 == getMy(cur_peaks, 1) and p3 == getMy(cur_peaks, 2):
		pass
	else:
		if intStart is not None:
			# Would normally use i+9, but that does not correspond to example outputs
			printPitch(intStart, i, p1, p2, p3)
		
		p1 = getMy(cur_peaks, 0)
		p2 = getMy(cur_peaks, 1)
		p3 = getMy(cur_peaks, 2)
		
		if p1 is not None:
			intStart = i
		else:
			intStart = None
	
if intStart is not None:
	# Would normally use numwindows+9, but that does not correspond to example outputs
	printPitch(intStart, numwindows, p1, p2, p3)

	