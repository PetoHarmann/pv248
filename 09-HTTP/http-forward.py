import sys, json
import http.server
import urllib.request
from urllib.parse import urlparse, parse_qs
from socket import timeout as timeout_exept
import ssl

import urllib.error



def parseUrl(url):
	if url.startswith('http://'):
		return (url[7:].split('/')[0], url.rstrip('/'))
	elif url.startswith('https://'):
		return (url[8:].split('/')[0], url.rstrip('/'))
	else:
		return (url.split('/')[0], 'http://' + url.rstrip('/'))

def URLHttpsToHttp(url):
	if url.startswith('https://'):
		return 'http://' + url[8:]
	else:
		return url
		
port = int(sys.argv[1])
upstream = sys.argv[2]
upstream_host, upstream_url = parseUrl(upstream)
global_socket = None

def SSLServNameHack(socket, name, context):
	global global_socket
	global_socket = socket

def DoRequest(url, headers, timeout = 1, method = 'GET', data = None, validate = True):
	output_json = {}
	
	global global_socket
	global_socket = None
	
	ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
	if validate == True:
		ssl_context.verify_mode = ssl.CERT_REQUIRED
		ssl_context.check_hostname = True
	else:
		ssl_context.verify_mode = ssl.CERT_NONE
		ssl_context.check_hostname = False
	
	ssl_context.load_default_certs()
	ssl_context.set_servername_callback(SSLServNameHack)
	
	req = urllib.request.Request(url = url, headers = headers, data = data, method = method)
	try:
		with urllib.request.urlopen(req, timeout = timeout, context = ssl_context) as response:
			peer_cert = None
			if global_socket is not None:
				peer_cert = global_socket.getpeercert()
			
			resp_headers = response.getheaders()
			resp_content = response.read()
				
			resp_headers_dict = {}
			for i in resp_headers:
				resp_headers_dict[i[0]] = i[1]
				
			try:
				content_json = json.loads(str(resp_content, 'utf-8'))
					
				output_json = {
					"code" : response.status,
					"headers" : resp_headers_dict,
					"json" : content_json
				}
			except:
				output_json = {
					"code" : response.status,
					"headers" : resp_headers_dict,
					"content" : str(resp_content)
				}
				
			if validate == True and peer_cert is not None:
				output_json['certificate valid'] = True
				hostname_list = []
				for k, i in enumerate(peer_cert['subject']):
					if i[0][0] == 'commonName':
						hostname_list.append(i[0][1])
				for k, i in enumerate(peer_cert['subjectAltName']):
					hostname_list.append(i[1])
				output_json['certificate for'] = hostname_list
			
	except (timeout_exept) as error:
		output_json = {
			'code' 		: 'timeout'
		}
	except (urllib.error.HTTPError) as error:
		output_json = {
			'code' 		: error.code,
		}
	except (urllib.error.URLError) as error:
		if validate == True and isinstance(error.reason, ssl.SSLError):
			print(str(error.reason))
			output_json = DoRequest(url, headers, timeout, method, data, validate = False)
			output_json['certificate valid'] = False
			return output_json
		else:	
			output_json = {
				'code' 		: str(error.reason)
			}
		
	return output_json

class MyHandler(http.server.BaseHTTPRequestHandler):
	def do_HEAD(self):
		self.send_response( 405, 'HEAD not supported by forwarder' )
		
	def do_GET(self):
		headers = {}
		
		for i in str(self.headers).split('\n'):
			if len(i) > 1:
				t = i.split(':')
				if(t[0] == "Host" and t[1].strip() == 'localhost'):
					headers[t[0]] = upstream_host
				else:
					headers[t[0]] = t[1].strip()
		
		output_json = DoRequest(upstream_url + self.path, headers)
		content = json.dumps(output_json, indent=4, separators=(',', ': '), ensure_ascii=False)
			
		self.send_response(200, 'OK')
		self.send_header( 'Connection', 'close' )
		self.send_header( 'Content-Type', 'text/json; charset=utf-8' )
		self.end_headers()
			
		self.wfile.write(bytes(content, 'utf-8'))
		
	def do_POST(self):
		input_content = None
		if 'Content-Length' in self.headers:
			length = int(self.headers['Content-Length'])		
			input_content = self.rfile.read(length)
		else:
			input_content = self.rfile.read()
		
		type		= 'GET'
		url			= None
		headers		= {}
		data 		= None
		timeout		= 1
		
		content = None
		
		try:
			input_json = json.loads(str(input_content, 'utf-8'))
		
			if 'type' in input_json:
				type 		= input_json['type']
			if 'url' in input_json:
				_, url 		= parseUrl(input_json['url'])
			if 'headers' in input_json:
				headers 	= input_json['headers']
			if 'data' in input_json:
				data		= input_json['content']
			if 'timeout' in input_json:
				timeout 	= input_json['timeout']
		except:
			pass

		if url is None or (type == 'POST' and data is None):
			output_json = {
				'code' 		: 'invalid json'
			}
				
			content = json.dumps(output_json, indent=4, separators=(',', ': '), ensure_ascii=False)
		else:
			output_json = DoRequest(url, headers, timeout, type, data)
			content = json.dumps(output_json, indent=4, separators=(',', ': '), ensure_ascii=False)
			
		self.send_response(200, 'OK')
		self.send_header( 'Connection', 'close' )
		self.send_header( 'Content-Type', 'text/json; charset=utf-8' )
		self.end_headers()
			
		self.wfile.write(bytes(content, 'utf-8'))

def run(server_class=http.server.HTTPServer, handler_class=http.server.BaseHTTPRequestHandler, port = 9001):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

run(handler_class = MyHandler, port=port)
	