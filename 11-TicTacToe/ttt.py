import sys, json
import http.server
import urllib.request
from urllib.parse import urlparse, parse_qs
from socket import timeout as timeout_exept

port = int(sys.argv[1])
games = {}
next_id = 1

class TTTExcept(Exception):
	def __init__(self, code, text):
		self.code = code
		self.text = text
		
	def __int__(self):
		return self.code
	
	def __str__(self):
		return self.text

def GameFactory(name):
	game = Game(name)
	global games
	global next_id
	games[next_id] = game
	next_id += 1
	return next_id - 1

def parseQuery(query):
	ret_q = parse_qs(query)
	for i, v in enumerate(ret_q):
		if len(ret_q[v]) != 1:
			raise TTTExcept(5, 'Multiple values for key! ' + str(v))
		ret_q[v] = ret_q[v][0]
	return ret_q
	
def getParam(query, name):
	if name in query:
		return query[name]
	raise TTTExcept(3, 'Parameter ' + name + ' not found in query.')

def getPramOpt(query, name, default = None):
	if name in query:
		return query[name]
	return default
	
class Game:
	def __init__(self, name):
		self.board = [[0,0,0],[0,0,0],[0,0,0]]
		self.turn = 1
		self.win = None
		self.name = name
		self.state = 1
		
	def getStatus(self):
		return (self.win, self.turn, self.board)
	
	def checkDraw(self):
		num = 0
		for i in range(0, 3):
			for j in range(0, 3):
				if self.board[i][j] == 0:
					num += 1
		if num == 0:
			self.win = 0
	
	def move(self, player, x, y):
		if x < 0 or x >= 3 or y < 0 or y >= 3:
			return 'Bad coordinates ' + str(x) + ', ' + str(y)
	
		if self.turn != player:
			return 'Not your turn'
	
		if self.board[x][y] == 0:
			self.board[x][y] = player
		else:
			return 'Square already taken'
		
		if self.turn == 2:
			self.turn = 1
		else:
			self.turn = 2
		
		l = 0
		r = 0
		d1 = 0
		d2 = 0
		for i in range(0, 3):
			if self.board[i][y] == player:
				l += 1
			if self.board[x][i] == player:
				r += 1
			if self.board[i][i] == player:
				d1 += 1
			if self.board[i][2-i] == player:
				d2 += 1
		if l == 3 or r == 3 or d1 == 3 or d2 == 3:
			self.win = player
			
		self.checkDraw()
		return None

class MyHandler(http.server.BaseHTTPRequestHandler):
	def do_HEAD(self):
		self.send_response( 405, 'HEAD not supported by forwarder' )
		
	def do_POST(self):
		self.send_response( 405, 'POST not supported by forwarder' )
		
	def do_GET(self):
		try:
			path = None
			query = None
			content = None
		
			path_comp = self.path.split('?')
			if len(path_comp) != 2:
				path = path_comp[0].strip('/')
				query = {}
			else:
				path = path_comp[0].strip('/')
				query = parseQuery(path_comp[1])
			
			if path == 'start':
				id = GameFactory(getPramOpt(query, 'name', ''))
				
				content = { 'id' : id }
			elif path == 'status':
				id = int(getParam(query, 'game'))
				winner, turn, board = games[id].getStatus()
			
				if winner is not None:
					content = { 'winner' : winner, 'board' : board }
				else:
					content = { 'board' : board, 'next' : turn }
			elif path == 'play':
				id = int(getParam(query, 'game'))
				player = int(getParam(query, 'player'))
				x = int(getParam(query, 'x'))
				y = int(getParam(query, 'y'))
				
				ret = games[id].move(player, x, y)
				
				if ret is None:
					if player == 2:
						games[id].state = 3
					content = { 'status' : 'ok' }
				else:
					content = { 'status' : 'bad', 'message' : ret }
			elif path == 'list':
				r_games = []
				for i in games:
					r_games.append({ 'id' : i, 'name' : games[i].name, 'state' : games[i].state })
				content = { 'games' : r_games}
			elif path == 'join':
				id = int(getParam(query, 'game'))
				if id not in games:
					content = { 'status' : 'bad id', 'message' : 'Invalid id of game' }
				elif games[id].state > 1:
					content = { 'status' : 'game full', 'message' : 'The game is already full' }
				else:
					games[id].state = 2
					content = { 'status' : 'ok', 'id' : id, 'player' : 2 }
			else:
				content = { 'nope' : path }
				
			self.send_response(200, 'OK')
			self.send_header( 'Connection', 'close' )
			self.send_header( 'Content-Type', 'text/json; charset=utf-8' )
			self.end_headers()
				
				
			content_json = json.dumps(content, indent=4, separators=(',', ': '), ensure_ascii=False)
			self.wfile.write(bytes(content_json, 'utf-8'))
		except TTTExcept as e:
			self.send_response(400, 'ERROR')
			self.send_header( 'Connection', 'close' )
			self.send_header( 'Content-Type', 'text/text; charset=utf-8' )
			self.end_headers()
			
			self.wfile.write(bytes(str(e), 'utf-8'))
		except Exception as e:
			self.send_response(400, 'ERROR')
			self.send_header( 'Connection', 'close' )
			self.send_header( 'Content-Type', 'text/text; charset=utf-8' )
			self.end_headers()

def run(server_class=http.server.HTTPServer, handler_class=http.server.BaseHTTPRequestHandler, port = 9001):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

run(handler_class = MyHandler, port=port)


