
import pandas, sys, json

points = pandas.read_csv(sys.argv[1])
points_porc = {}

if sys.argv[2] == 'dates':
	for i, d in enumerate(points):
		if i == 0:
			continue
		date = d.split('/')[0]
		
		if date in points_porc:
			points_porc[date] += points[d]
		else:
			points_porc[date] = points[d]
elif sys.argv[2] == 'exercises':
	for i, d in enumerate(points):
		if i == 0:
			continue
		exer = d.split('/')[1]
		
		if exer in points_porc:
			points_porc[exer] += points[d]
		else:
			points_porc[exer] = points[d]
elif sys.argv[2] == 'deadlines':
	for i, d in enumerate(points):
		if i == 0:
			continue
		
		points_porc[d] = points[d]
else:
	print('Bad input!')

output_json = {}
	
for i, d in points_porc.items():
	output_json[i] = {
		'mean' : float(d.mean()),
		'median' : float(d.median()),
		'first' : int(d.quantile(0.25)),
		'last' : int(d.quantile(0.75)),
		'passed' : int((d>0.0).sum())
	}
	
print(json.dumps(output_json, indent=4, separators=(',', ': '), ensure_ascii=False))

