
import pandas, sys, json, numpy, datetime
# from scipy import stats
from scipy import optimize

# Get the required raw input
points = pandas.read_csv(sys.argv[1])
student = None

if sys.argv[2] == 'average':
	student = points.mean()
else:
	student = points[points['student'] == int(sys.argv[2])].iloc[0]
	
# Helpet functions
semester_start_date = datetime.date(2018, 9, 17)
def DaysSinceSemesterStart(date):
	decomp = date.split('-')
	date_obj = datetime.date(int(decomp[0]), int(decomp[1]), int(decomp[2]))
	return (date_obj - semester_start_date).days
	
def AddToRecords(day_points, day, points):
	if day in day_points:
		day_points[day] += points
		
		for k, v in day_points.items():
			if k > day:
				day_points[k] += points
			
	else:
		max = 0.0
		for k, v in day_points.items():
			if k > day:
				day_points[k] += points
			elif v > max:
				max = v
		
		day_points[day] = points + max
	
	
# Vars for Processed data
points_porc = {}
day_points = { 0:0 }

# Process the raw data
for d, v in student.items():
	if d == 'student':
		continue
	date = d.split('/')[0]
	exer = d.split('/')[1]
	
	AddToRecords(day_points, DaysSinceSemesterStart(date), float(v))
	
	if exer in points_porc:
		points_porc[exer] += float(v)
	else:
		points_porc[exer] = float(v)

values = numpy.array(list(points_porc.values()))
		
# Compute regression slope the ugly way, to fix intercept at 0
regression_slope = optimize.curve_fit(lambda x, m: m*x, list(day_points.keys()), list(day_points.values()))[0][0]
#regression_slope = stats.linregress(list(day_points.keys()), list(day_points.values()))[0]

# Prepare Output Dict
output = {
	'mean':	numpy.mean(values),
	'median': numpy.median(values),
	'passed': int(numpy.sum(values > 0.0)),
	'total': float(numpy.sum(values)),
	'regression slope': regression_slope
}

if regression_slope > 0.0:
	output['date 16'] = str(semester_start_date + datetime.timedelta(days = int(16 / regression_slope)))
	output['date 20'] = str(semester_start_date + datetime.timedelta(days = int(20 / regression_slope)))

# Print Output
print(json.dumps(output, indent=4, separators=(',', ': '), ensure_ascii=False))

#print(stats.linregress(list(day_points.keys()), list(day_points.values()))[0])
# print(optimize.curve_fit(lambda x, m: m*x, list(day_points.keys()), list(day_points.values()))[0][0])
	
	