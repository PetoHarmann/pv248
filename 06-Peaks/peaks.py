import wave, struct, numpy, math, sys

f = wave.open(sys.argv[1], 'rb')

data = []
if f.getnchannels() == 1:
	frames = f.readframes(f.getnframes())
	for i in range(0, f.getnframes()*2, 2):
		data.append(struct.unpack("h", bytearray(frames[i:(i+2)]))[0])
elif f.getnchannels() == 2:
	frames = f.readframes(f.getnframes())
	for i in range(0, f.getnframes()*4, 4):
		data.append((struct.unpack("h", bytearray(frames[i:(i+2)]))[0] + struct.unpack("h", bytearray(frames[(i+2):(i+4)]))[0])/2)

framerate = f.getframerate()
numwindows = math.floor(f.getnframes()/f.getframerate())
maxarray = []

for i in range(0, numwindows):
	win = data[i*framerate:(i*framerate+framerate)]
	proc = numpy.abs(numpy.fft.rfft(win))
	mean = numpy.mean(proc)
	for i in range(0,len(proc)):
		if(proc[i] >= 20 * mean):
			maxarray.append(i)


if(len(maxarray) > 0):
	print("low = " + str(numpy.min(maxarray)) + ", high = " + str(numpy.max(maxarray)))
else:
	print("no peaks")
	